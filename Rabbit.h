#ifndef RABBIT_H
#define RABBIT_H
#include <iostream>
#include <string>
#include <vector> // Mostly used for defect array
using namespace std;

// An enum used for the gender of the rabbit
enum Gender { MALE, FEMALE, UNKNOWN }; // "Unknown" will likely be for special/mutant rabbits

// Enum used to indicate certain defects, whether acquired or by birth
enum Defect { MISSING_LEG, MISSING_EAR, SICKNESS, PLAGUE, ZOMBIE }; // TODO: expand list and give comment on each defect

// enum FurColor {};

#endif